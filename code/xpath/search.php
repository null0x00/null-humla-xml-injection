<?php
$result = '';
if(isset($_POST['submit'])){
$doc = new DOMDocument;
$doc->load('emp.xml');
$xpath = new DOMXPath($doc);
//$query = "/Employees/Employee[UserName='' or '1'='1']";
$input = $_POST['query'];
$query = "/Employees/Employee[@ID='".$input."']";
$result = $xpath->query($query);
}
?>
<!DOCTYPE html>
<html>
<head>
  <title>XPath Injections</title>
</head>
<body>
  <center>
  <h1>XPath Injection</h1>
  <form action="" method="POST">
   <b>Query:</b> <input type="text" name="query" size="100" value="<?php echo $input;?>"/><p>
    <input type="submit" name="submit" value="Submit"/>
  </form>
 <h2>Output:</h2>
<pre>
<?php
foreach($result as $row){
  echo "FirstName: ".$row->getElementsByTagName('FirstName')->item(0)->nodeValue."<p>";
}
?></pre>
  </center>
</body>
</html>
