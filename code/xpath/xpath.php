<?php
error_reporting(0);
$result = '';
if(isset($_POST['submit'])){
$doc = new DOMDocument;
$doc->load('emp.xml');
$xpath = new DOMXPath($doc);
$query = $_POST['query'];
$result = $xpath->evaluate($query);
}
?>
<!DOCTYPE html>
<html>
<head>
  <title>XPath Injections</title>
</head>
<body>
  <center>
  <h1>XPath Injection</h1>
  <form action="" method="POST">
   <b>Query:</b> <input type="text" name="query" size="100" value="<?php echo $query;?>"/><p>
    <input type="submit" name="submit" value="Submit"/>
  </form>
<p><b>XPath Query: </b><?php echo $query;?><p>
 <h2>Output:</h2>
<pre>
<?php
if($result){
  foreach ($result as $row){
    echo $row->textContent."<p>";
  }
echo $result;
}
?></pre>
  </center>
</body>
</html>
